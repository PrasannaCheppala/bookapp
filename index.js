/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import PushNotification from "react-native-push-notification";

    PushNotification.configure({
      // (required) Called when a remote or local notification is opened or received
      onNotification: function (notification) {
          console.log('LOCAL NOTIFICATION ==>', notification)
      },
  
      popInitialNotification: true,
      requestPermissions: Platform.OS === 'ios'
  })
  
  PushNotification.createChannel(
      {
          channelId: "channel-id", // (required)
          channelName: "Channel", // (required)
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
  );
  

AppRegistry.registerComponent(appName, () => App);
