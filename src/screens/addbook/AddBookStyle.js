import {StyleSheet} from 'react-native'

export default StyleSheet.create({
    root: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    account:{
        margin:20,
        fontSize:20,
        fontWeight:'bold',
        padding:25
    },
    text: {
      color:'gray',
      marginVertical:10,
      width:'90%'

    },
    errorMsg: {
        color: 'red',
        marginBottom: 10,
        marginRight: 40,
    },
    link:{
        color:'#FDB075'
    }

})