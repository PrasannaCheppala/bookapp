import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView ,Alert} from 'react-native';
import CustomInput from '../../components/custominput/CustomInput';
import CustomButton from '../../components/custombutton/CustomButton';
import AddBookStyle from './AddBookStyle';
import PushNotification from 'react-native-push-notification';

const AddBook = () => {
    const [data,setData] =useState(
        {
            name:'',
            author:'',
            downloads:'',
            isValidName: true,
            isValidAuthor: true, 
            isValidPrice:true  
        }
    )
    
    const textInputChange = (val) => { 
        if (val.trim().length >=3) {
            setData({
                ...data,
                name: val,
                isValidName: true
            })
        } else {
            setData({
                ...data,
                userName: val,
                isValidName: false
            })
        } 
    }
  
    const textAuthorChange = (val) => { 
        if (val.trim().length >=4) {
            setData({
                ...data,
                author: val,
                isValidAuthor: true
            })
        } else {
            setData({
                ...data,
                author: val,
                isValidAuthor: false
            })
        } 
}

    const handlePriceChange = (val) => {
           setData({
               
                ...data,
                price: val,
                isValidPrice: true
            })
    }
   

    const onAddPressed = () => {
       
       
        
        let collection ={}
        collection.name=data.name;
        collection.author=data.author;
        collection.price=data.price;
        if (collection.name.length === 0 || collection.author.length === 0 || collection.price.length===0)  {
            Alert.alert('Wrong Input!', 'Book Name  or Author or Price field cannot be empty.', [
                { text: 'Okay' }
            ]);
            return;
        }
        var url ='https://61dddb4af60e8f0017668ac5.mockapi.io/api/v1/Book';    
        fetch(url,{
            method:'POST',
            body:JSON.stringify(collection),
            headers:new Headers({'Content-type':'application/json'})
        }).then(res=>res.json())
        .catch(error=>console.error('Error',error))
        .then(response=>
            console.log('Success',response))
            Alert.alert('Success','Book added Successfully.',[
                {text:'Okay'}
            ]);
            PushNotification.localNotification({
            
                channelId:'channel-id',
                title:'You Added Book as ' + data.name,
                message:'Author is ' + data.author + ' and Price is ' + data.price
                
            });
            
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={AddBookStyle.root}>
                <Text style={AddBookStyle.account}>Add a New Book</Text>
                <CustomInput
                    placeholder={"Name"}
                    onChangeText={(val) => textInputChange(val)}
                />
                {data.isValidName ? null : 
                <Text style={AddBookStyle.errorMsg}> Book name must be more than 4 letters.</Text>}

                <CustomInput
                    placeholder={"Author"}
                    onChangeText={(val) => textAuthorChange(val)}
                />
                {data.isValidAuthor ? null :
                 <Text style={AddBookStyle.errorMsg}> Author name must be more than 4 letters.</Text>}

                <CustomInput
                    placeholder={"Price"}
                    keyboardType = 'numeric' 
                    onChangeText={(val) => handlePriceChange(val)}
                />
                <CustomButton   text="Add" onPress={onAddPressed} />

            </View>
        </ScrollView>
    )
}


export default AddBook;