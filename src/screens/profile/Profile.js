import { View, StyleSheet , SafeAreaView, TouchableOpacity} from 'react-native';
import React ,{useContext} from 'react';
import { Avatar, Title, Caption,Text, TouchableRipple } from 'react-native-paper';


import { AuthContext } from '../../services/AuthContext';
const Profile = () => {
  const { signOut } = React.useContext(AuthContext);
  
  return (
    <SafeAreaView style={styles.container}>
    <View style={styles.userInfoSection}>
    <View style={{flexDirection:'row', marginTop:15}}>
      <Avatar.Image
      source={require('../../../assets/images/book_1.jpg')}
      size={70}/>
     
    <View style ={{marginLeft:20  }}>
    <Title style={[styles.title,{marginTop:15,marginBottom:5,}]}>prasanna</Title>
    <Caption style={styles.caption}>prasanna@gmail.com</Caption>
    </View>
    </View>
    </View>
    <View style={styles.userInfoSection}>
      <View style={styles.row}>
        <Text>Hyd, India</Text>
      </View>
      <View style={styles.row}>
     
        <Text>1234567890</Text>
      </View>
    </View>
    <View>
      <TouchableOpacity onPress={signOut}>
      <Text style={styles.logout}>Logout</Text>
      </TouchableOpacity>
    </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({

  container:{
    margin:10
  },
  icon:{
    marginLeft:30,

  },
  logout: {
    backgroundColor: '#C70039',
    color: 'white',
    fontSize: 16,
    width: 100,
    marginVertical: 15,
    borderColor: '#C70039',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    borderWidth: 1,
    borderRadius: 10,

},
})

export default Profile;
