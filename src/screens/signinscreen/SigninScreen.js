import React, { useState, useContext, } from 'react';
import { View, Text, Image, StyleSheet, useWindowDimensions, ScrollView, Alert } from 'react-native';
import image from '../../../assets/images/logo.jpeg';
import CustomInput from '../../components/custominput/CustomInput';
import CustomButton from '../../components/custombutton/CustomButton';
import { useNavigation } from '@react-navigation/native';
import { Icon } from 'react-native-elements';
import { AuthContext } from '../../services/AuthContext';
import users from '../../../model/users';
import SignUpScreen from '../signupscreen/SignUpScreen'

const SigninScreen = () => {

    const [data, setData] = useState({
        userName: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
    })

    const { signIn } = React.useContext(AuthContext)
    const navigation = useNavigation()

    const textInputChange = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                userName: val,
                check_textInputChange: true,
                isValidUser: true
            })
        } else {
            setData({
                ...data,
                userName: val,
                check_textInputChange: true,
                isValidUser: false
            })
        }

    }
    const handlePasswordChange = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                password: val,
                check_textInputChange: true,
                isValidPassword: true

            });
        } else {
            setData({
                ...data,
                password: val,
                check_textInputChange: true,
                isValidPassword: false

            });

        }
    }
    const onForgotPressed = () => {
        console.warn("Forgot in")
    }
    const onSignUpPressed = () => {
        navigation.navigate('SignUp')
    }
    const loginHangle = (userName, password) => {
        const foundUser = users.filter(item => {
            return userName === item.username && password === item.password;
        });
        if (data.userName.length === 0 || data.password.length === 0) {
            Alert.alert('Wrong Input!', 'Username or password field cannot be empty.', [
                { text: 'Okay' }
            ]);
            return;
        }
        if (foundUser.length === 0) {
            Alert.alert('Invalid User!', 'Username or password is incorrect.', [
                { text: 'Okay' }
            ]);
            return;
        }
        signIn(foundUser)
        console.log('username', userName, password);

    }
    const handleValidUser = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                isValidUser: true,
            })
        } else {
            setData({
                ...data,
                isValidUser: false,

            })
        }
    }
    const handleValidPassword = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                isValidPassword: true,
            })
        } else {
            setData({
                ...data,
                isValidPassword: false


            })
        }
    }


    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.root}>
                <Image source={image}
                    style={[styles.logo]}
                    resizeMode='contain' />

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Icon name="person" size={28} color="gray" style={styles.icon} />
                    <CustomInput
                        placeholder={"Username"}
                        onChangeText={(val) => textInputChange(val)}
                        onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                    />
                </View>
                {data.isValidUser ? null : <Text style={styles.errorMsg}> Username must be 4 characters long.</Text>}

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Icon name="lock" size={28} color="gray" style={styles.icon} />
                    <CustomInput
                        placeholder={"Password"}
                        secureTextEntry={true}
                        onChangeText={(val) => handlePasswordChange(val)}
                        onEndEditing={(e) => handleValidPassword(e.nativeEvent.text)}
                    />
                </View>
                {data.isValidPassword ? null : <Text style={styles.errorMsg}> Password must be 4 characters long.</Text>}

                <CustomButton text="Sign in" onPress={() => { loginHangle(data.userName, data.password) }} />
                <CustomButton text="Forgot Password?" onPress={onForgotPressed} type='TERTIARY' />
                <CustomButton text="Don't have an account? Create One" onPress={onSignUpPressed} type='TERTIARY' />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    errorMsg: {
        color: 'red',
        marginBottom: 10,
        marginRight: 120,
    },
    root: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        maxWidth: 300,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 30
    },
    icon: {
        backgroundColor: 'white',
        padding: 10,
        borderColor: '#e8e8e8',
        borderWidth: 0.5,
        borderRadius: 5,
        marginVertical: 5,
    }


})
export default SigninScreen;