import { StyleSheet, Text, View } from 'react-native';
import React from 'react';

const BottomTab = () => {

   
const bottomIcons = [
    <TouchableOpacity onPress={addBookPressed}>
             <Icon name="home"  size={28}  />
            </TouchableOpacity>,

             <TouchableOpacity onPress={addBookPressed}>
            <Icon name="person"  size={28} />
             </TouchableOpacity>,

              <TouchableOpacity >
              <Icon name="logout"  size={28} /> 
              </TouchableOpacity>
    
];
const [bottomTab, setBottomTab] = React.useState(0)

const BottomList = ({ icon}) => {
    return (
        <View style={styles.bottomTabContainer}>
            {bottomIcons.map((item, index) => (
                <TouchableOpacity
                    activeOpacity={0.8}
                    key={index}
                    onPress={() => setBottomTab(index)}
                >
                    <Text style={[styles.categoryText, bottomTab== index && styles.bottomTabSelected]}>{item}</Text>
                </TouchableOpacity>
            ))}

        </View>
    );
}
return(
    <BottomList/>
)
};

const styles = StyleSheet.create({
    bottomTabContainer: {
        flexDirection: 'row',
        marginBottom: 5,
        marginTop: 8,
        borderRadius:10,
        height:40,
        alignItems:'center',
        justifyContent:'center',
        color:'white',
        justifyContent: 'space-between'
    },
    bottomTabSelected: {
        color: 'brown',
        paddingBottom: 5,
        borderBottomWidth: 2,
        borderColor: 'brown',
        
    },
});
export default BottomTab;


