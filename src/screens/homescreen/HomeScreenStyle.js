import {StyleSheet} from 'react-native'



export default StyleSheet.create({
    root:{
        flex: 1, paddingHorizontal: 20, 
    },
    header: {
        flexDirection: 'row',
        mariginTop: 20,
        justifyContent: 'space-between',
      
    },
    logout: {
        backgroundColor: '#C70039',
        color: 'white',
        fontSize: 16,
        width: 100,
        marginVertical: 15,
        borderColor: '#C70039',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderWidth: 1,
        borderRadius: 10,

    },
    searchContainer: {
        height: 50,
        borderRadius: 15,
        backgroundColor: '#E5DEDE',
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center'
    },
    cardText:{
        marginTop:10,
        fontWeight:'bold'
    },
    input: {
        fontSize: 18,
        color: 'black'
    },
    categoryContainer: {
        flexDirection: 'row',
        marginBottom: 5,
        marginTop: 8,
        justifyContent: 'space-between'
    },
    categoryText: {
        fontWeight: 'bold',
        color: 'gray',
        fontSize: 16
    },
    categoryTextSelected: {
        color: 'brown',
        paddingBottom: 5,
        borderBottomWidth: 2,
        borderColor: 'brown'
    },
   
    themeView:{
        
        flexDirection:'row',
        marginVertical:20
    }

})
