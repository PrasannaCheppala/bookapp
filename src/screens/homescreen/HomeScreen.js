import React, { useState, useEffect } from 'react'
import { View, Text, HomeScreenStyleheet, TextInput, Image, TouchableOpacity,FlatList, Dimensions, ActivityIndicator } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import { useNavigation,useTheme as NavigationTheme  } from '@react-navigation/native';
import CustomButton from '../../components/custombutton/CustomButton';
import { Icon } from 'react-native-elements';
import { AuthContext } from '../../services/AuthContext';
import AddBook from '../addbook'
import HomeScreenStyle from './HomeScreenStyle';
import {Switch,useTheme} from 'react-native-paper';


const width = Dimensions.get("screen").width / 2 - 30;

const HomeScreen = ({nav}) => {
    const {colors}=NavigationTheme();
    const paperTheme= useTheme();
    const [filteredData, setFilteredData] = useState([]);
    const [masterData, setMasterData] = useState([]);
    const [search, setSearch] = useState('');
    const [isLoading, setIsloading] = useState(true);
    const navigation = useNavigation()
   
    const { signOut ,toggleTheme} = React.useContext(AuthContext);
   
    // const toggleTheme =()=>{
    //     setIsDarkTheme(!isDarkTheme);
    // }

    useEffect(() => {
        fetchPosts();
        return () => {

        }
    }, [])
   

    const fetchPosts = () => {
        const apiURL = 'https://61dddb4af60e8f0017668ac5.mockapi.io/api/v1/Book';
        fetch(apiURL)
            .then((response) =>
                response.json())
            .then((responseJson) => {
              setIsloading(false)
                setFilteredData(responseJson);
                setMasterData(responseJson)
            }).catch((error) => {
                setIsloading(false)
                console.error(error);
            })
    }
   

    const searchFilter = (text) => {
        if (text) {
        
            const newData = masterData.filter((item) => {      
                const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase();
               return itemData.indexOf(textData) > -1;
              
            });
         
            setFilteredData(newData);
            setSearch(text);
        }
        else {
            setFilteredData(masterData);

            setSearch(text);
        }
    }

    const onLogoutPressed = () => {
        console.warn("Logout preseed")
    }
    const addBookPressed = () => {
        navigation.navigate('AddBook')
    }

    const categories = [' POPULAR', 'MYSTERY', 'POETRY', 'THRILLER'];
    const [categoryIndex, setCategoryIndex] = React.useState(0)
    const CategoryList = ({ navigation }) => {
        return (
            <View style={HomeScreenStyle.categoryContainer}>
                {categories.map((item, index) => (
                    <TouchableOpacity
                        activeOpacity={0.8}
                        key={index}
                        onPress={() => setCategoryIndex(index)}
                    >
                        <Text style={[HomeScreenStyle.categoryText, categoryIndex == index && HomeScreenStyle.categoryTextSelected],{color:colors.text}}>{item}</Text>
                    </TouchableOpacity>
                ))}

            </View>
        );
    }

    

    const Card = ({ books }) => {
        return (
            <TouchableOpacity>
                <View style={{ height: 250,  backgroundColor: '#E5DEDE', width,  marginHorizontal: 2,  borderRadius: 10,  marginBottom: 20,  padding: 15}}>
                    <View style={{ height: 100, alignItems: 'center' }}>
                        <Image style={{ flex: 2, resizeMode: 'contain' }}
                            source={require('../../../assets/images/book_1.jpg')}
                        />
                    </View>

                    <Text style={HomeScreenStyle.cardText}>
                        Name: {books.name}
                    </Text>
                    <Text style={HomeScreenStyle.cardText}>
                        Author: {books.author}
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={HomeScreenStyle.cardText}>
                            Downloads:{'  '}
                        </Text>
                        <Text style={[HomeScreenStyle.cardText],{ color: 'black' ,marginTop:10}}>
                            {books.downloads}
                        </Text>
                    </View>
                   
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 1
                    }}>
                        <Text ></Text>
                        <View >
                            <Text style={
                            [HomeScreenStyle.cardText]}>
                                Price: ${books.price}
                            </Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    };
    if (isLoading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size={'large'} color={"brown"}></ActivityIndicator>
            </View>
        );
    }
    return (
        <SafeAreaView
            style={HomeScreenStyle.root
            }>
            <View style={HomeScreenStyle.header}>
                <View>
                    <Text style={{ fontSize: 25, fontWeight: 'bold', color:colors.text }}>Welcome to</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 38, color: 'brown' }}>Book Store </Text>
                </View>
                
                <TouchableOpacity onPress={()=>{toggleTheme()}} >
               
                <View style={HomeScreenStyle.themeView}>
                    <Text  style={{color:colors.text, }}>DarkTheme</Text>
                    <View pointerEvents='none'>
                        <Switch value={paperTheme.dark}/>
                    </View>
                </View>
                </TouchableOpacity>
               
                    
                
            </View>

            <View style={{ marginTop: 10, flexDirection: 'row' }}>
                <View style={HomeScreenStyle.searchContainer}>
                    <Icon name="search" size={28} color="#887700" style={{ marginLeft: 20 }} />
                    <TextInput
                        placeholder='search for name of the book'
                        style={HomeScreenStyle.input}
                        value={search}     
                        onChangeText={(text) => searchFilter(text)}
                    ></TextInput>
                </View>
            </View>

            <CategoryList />

            <FlatList
                columnWrapperStyle={{ justifyContent: 'space-between' }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    marginTop: 10,
                    paddingBottom: 50
                }}
                numColumns={2}
                data={filteredData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => <Card books={item} />} />
        </SafeAreaView>

    )
}

export default HomeScreen