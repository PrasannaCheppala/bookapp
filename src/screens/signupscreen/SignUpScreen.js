import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import image from '../../../assets/images//logo.jpeg';
import CustomInput from '../../components/custominput/CustomInput';
import CustomButton from '../../components/custombutton/CustomButton';
import { useNavigation } from '@react-navigation/native';

const SignUpScreen = () => {
    const navigation = useNavigation()
    const [data, setData] = useState(
        {
            userName: '',
            password: '',
            email: '',
            passwordRepeat: ''

        }
    )

    const textInputChange = (val) => {
        setData({
            ...data,
            userName: val,
        })
    }
    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val,
        });
    }
    const handlePasswordRepeatChange = (val) => {
        setData({
            ...data,
            passwordRepea: val,
        });
    }
    const handleEmailChange = (val) => {
        setData({
            ...data,
            email: val,
        });
    }

    const onRegisterPressed = () => {
        let collection = {}
        collection.userName = data.userName;
        collection.password = data.password;
        console.warn(collection)
    }

    const onTermsPressed = () => {
        console.warn("Terms")
    }

    const onPrivacyPressed = () => {
        console.warn("Privacy")
    }
    const onFacebookPressed = () => {
        console.warn("Facebook")
    }

    const onGooglePressed = () => {
        console.warn("Google")
    }

    const onSignInPressed = () => {
        navigation.navigate('Signin')

    }
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.root}>
                <Text style={styles.account}>Create an account</Text>
                <CustomInput
                    placeholder={"UserName"}
                    onChangeText={(val) => textInputChange(val)}
                />

                <CustomInput
                    placeholder={"Email"}
                    onChangeText={(val) => handleEmailChange(val)}

                />

                <CustomInput
                    placeholder={"Password"}
                    onChangeText={(val) => handlePasswordChange(val)}
                    secureTextEntry={true}
                />

                <CustomInput
                    placeholder={"Repeat Password"}
                    onChangeText={(val) => handlePasswordRepeatChange(val)}
                    secureTextEntry={true}
                />
                <CustomButton text="Register" onPress={onRegisterPressed} />
                <Text style={styles.text}>By registering, you confirm that ypu accept our{' '}
                    <Text style={styles.link} onPress={onTermsPressed}>Terms of Use</Text> {' '}and {' '}
                    <Text style={styles.link} onPress={onPrivacyPressed}>Privacy Policy</Text>  </Text>

                <CustomButton text="Sign In with Facebook" onPress={onFacebookPressed} type='TERTIARY' fgColor={'#4765A9'} bgColor={'#E7EAF4'} />
                <CustomButton text="Sign In with Google" onPress={onGooglePressed} type='TERTIARY' fgColor={'#DD4D44'} bgColor={'#FAE9EA'} />
                <CustomButton text="Have an account? Sign In" onPress={onSignInPressed} type='TERTIARY' />

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    root: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    account: {
        margin: 20,
        fontSize: 20,
        fontWeight: 'bold',
        padding: 25
    },
    text: {
        color: 'gray',
        marginVertical: 10,
        width: '90%'

    },
    link: {
        color: '#FDB075'
    }

})
export default SignUpScreen;