import { NavigationContainer, 
    DarkTheme as NavigationDarkTheme ,
    DefaultTheme as NavigationDefaultTheme} from '@react-navigation/native'
import React, { useEffect, useState } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { Icon } from 'react-native-elements';
import { AuthContext } from '../services/AuthContext';
import AsyncStorage from '@react-native-community/async-storage';
import SignUpScreen from '../screens/signupscreen/SignUpScreen';
import SigninScreen from '../screens/signinscreen/SigninScreen';
import { Provider as PaperProvider, 
    DefaultTheme as PaperDefaultTheme,
    DarkTheme as PaperDarkTheme } from 'react-native-paper';
import HomeScreen from '../screens/homescreen/HomeScreen';
import AddBook from '../screens/addbook';
import Profile from '../screens/profile/Profile';
import { createNativeStackNavigator } from '@react-navigation/native-stack'


const stack = createNativeStackNavigator();
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();


const Navigation = () => {

    const [isDarkTheme, setIsDarkTheme] =React.useState(false);
    const intialLoginState = {
        isloading: true,
        userName: null,
        userToken: null

    };
    const CustomDefaultTheme={
        ...NavigationDefaultTheme,
        ...PaperDefaultTheme,
        colors:{
        ...NavigationDefaultTheme.colors,
        ...PaperDefaultTheme.colors,
        background: '#ffffff',
        text:'#333333'
        }
    };

    const CustomDarkTheme={
        ...NavigationDarkTheme,
        ...PaperDarkTheme,
        colors:{
        ...NavigationDarkTheme.colors,
        ...PaperDarkTheme.colors,
        background:'#333333',
        text:'#ffffff'
        }
    };

    const theme= isDarkTheme ? CustomDarkTheme : CustomDefaultTheme
    const loginReducer = (prevState, action) => {
        switch (action.type) {
            case 'RETRIEVE_STATE':
                return {
                    ...prevState,
                    userToken: action.token,
                    isloading: false
                }
            case 'LOGIN':
                return {
                    ...prevState,
                    userName: action.id,
                    userToken: action.token,
                    isloading: false
                }

            case 'LOGOUT':
                return {
                    ...prevState,
                    userName: null,
                    userToken: null,
                    isloading: false
                }

            case 'REGISTER':
                return {
                    ...prevState,
                    userName: action.id,
                    userToken: action.token,
                    isloading: false
                }
        }
    }

    const [loginState, dispatch] = React.useReducer(loginReducer, intialLoginState)
    const authContext = React.useMemo(() => ({
        signIn: async (foundUser) => {
            const userToken = String(foundUser[0].userToken);
            const userName = foundUser[0].username;

            try {
                await AsyncStorage.setItem('userToken', userToken)
            } catch (e) {
                console.log(e)
            }
            console.log('user Token signin', userToken)
            dispatch({ type: 'LOGIN', id: userName, token: userToken })
        },
        signOut: async () => {
            try {
                await AsyncStorage.removeItem('userToken')
            } catch (e) {
                console.log(e)
            }
            dispatch({ type: 'LOGOUT' })
        },
        signUp: () => {
            // setIsloading(false),
            // setUserToken('abcd')
        },
        toggleTheme:()=>{
            setIsDarkTheme(isDarkTheme=>!isDarkTheme)
        }

    }), []);

    useEffect(
        () => {
            setTimeout(async () => {
                let userToken;
                userToken = null;
                try {
                    userToken = await AsyncStorage.getItem('userToken')
                    console.log('user Token use effect', userToken)

                } catch (e) {
                    console.log(e)
                }
                dispatch({ type: 'REGISTER', token: userToken })
            }, 1000);
        }, []
    );
    if (loginState.isloading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size={'large'}></ActivityIndicator>
            </View>
        );
    }


    return (
        <PaperProvider theme={theme}>
        <AuthContext.Provider value={authContext}>
            <NavigationContainer theme={theme}>
                {loginState.userToken !== null ? (
                    
                    <Tab.Navigator screenOptions={{ activeTintColor: "brown", inactiveTintColor: "gray"}}>
                        <><Tab.Screen name="Home" component={HomeScreen} options={{ headerShown: false, tabBarIcon: ({ color }) => (<Icon name="home" color={color} tvParallaxProperties={undefined} />) }} />
                            <Tab.Screen name="AddBook" component={AddBook} options={{ headerShown: false, tabBarIcon: ({ color }) => (<Icon name="library-books" color={color} tvParallaxProperties={undefined} />) }} />
                            <Tab.Screen name="Profile" component={Profile} options={{ headerShown: false, tabBarIcon: ({ color }) => (<Icon name="person" color={color} tvParallaxProperties={undefined} />) }} />
                        </>
                    </Tab.Navigator>
                ) :
                    <stack.Navigator>
                        <><stack.Screen name="Signin" style={{ color: '#ffffff' }} component={SigninScreen}
                            options=
                            {{ headerShown: false, showLabel: false, tabBarIcon: ({ color }) => (<Icon name="plus" type="evilicon" color={'#ffffff'} />) }}
                        />
                            <stack.Screen name="SignUp" style={{ color: '#ffffff' }} component={SignUpScreen} options=
                                {{ headerShown: false, showLabel: false, tabBarIcon: ({ color }) => (<Icon name="plus" type="evilicon" color={'#ffffff'} />) }}
                            />
                        </>
                    </stack.Navigator>
                }
            </NavigationContainer>
        </AuthContext.Provider>
        </PaperProvider>

    )
}

export default Navigation
