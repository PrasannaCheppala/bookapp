import React from 'react';
import { View, Text, Image, StyleSheet, TextInput, Button, Pressable } from 'react-native';


const CustomButton = ({ onPress, text, type = "PRIMARY", bgColor, fgColor }) => {
    return (
        <Pressable
            onPress={onPress}
            style={[
                styles.button,
                styles[`button_${type}`],
                bgColor ? { backgroundColor: bgColor } : {}

            ]}>
            <Text style={[
                styles.text,
                styles[`text_${type}`],
                fgColor ? { color: fgColor } : {}
            ]}>{text}</Text>


        </Pressable>
    )
}
const styles= StyleSheet.create({
    button: {
        width: '90%',
        padding: 13,
        borderRadius: 5,
        paddingHorizontal: 10,
        marginVertical: 5,
        alignItems: 'center'

    },
    button_PRIMARY: {
        backgroundColor: '#3871F3',
    },
    button_TERTIARY: {
    },
    text: {
        fontWeight: 'bold',
        color: 'white'
    },
    text_TERTIARY: {
        color: 'gray'
    },    
})


export default CustomButton;