import React from 'react';
import { View, Text, Image, StyleSheet, TextInput } from 'react-native';
const CustomInput = ({ value, setValue, onChangeText, placeholder,keyboardType, secureTextEntry, onEndEditing }) => {
    return (
        <View style={styles.container}>


            <TextInput
                value={value}
                onChangeText={onChangeText}
                setValue={setValue}
                placeholder={placeholder}
                keyboardType = {keyboardType}
                style={styles.input}
                secureTextEntry={secureTextEntry}
                onEndEditing={onEndEditing}
            />

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        width: '80%',
        borderColor: '#e8e8e8',
        borderWidth: 1,
        borderRadius: 5,
        paddingHorizontal: 10,
        marginVertical: 5,
        color: 'black'

    }

})
export default CustomInput;